---
title: FAQ
---

This FAQ changes regularly, and it always documents the latest version of LibreWolf. Please keep your browser up to date.

If your question is not answered here, you can try to get answers on [r/LibreWolf](https://www.reddit.com/r/LibreWolf/), or in our [Gitter](https://gitter.im/librewolf-community/librewolf)/[Matrix](https://app.element.io/#/room/#librewolf:matrix.org) room. Alternatively you could check the issues in the relevant [gitlab repositories](https://gitlab.com/librewolf-community).

The faq does not currently work with our Hugo site generator, so we had to migrate it:

# [Go to the FAQ](https://gitlab.com/librewolf-community/settings/-/wikis/FAQ).

