---
title: Docs
---

# LibreWolf Documentation

This project is an independent “fork” of Firefox, with the primary goals of privacy security and user freedom. It is the community run successor to [LibreFox](https://github.com/intika/Librefox)

LibreWolf is designed to minimize data collection and telemetry as much as possible. This is achieved through hundreds of privacy/security/performance settings and patches. Intrusive integrated addons including updater, crashreporter, and pocket are removed too.

**LibreWolf is NOT associated with Mozilla or its products.**

## Features

- **Latest Firefox** — LibreWolf is compiled directly from the latest build of Firefox Stable. You will have the the latest features, and security updates.
- **Independent Build** — LibreWolf uses a build independent of Firefox and has its own settings, profile folder and installation path. As a result, it can be installed alongside Firefox or any other browser.
- **No phoning home** — Embedded server links and other calling home functions are removed. In other words, *minimal background connections* by default.
- User settings updates
- Extensions firewall: limit internet access for extensions.
- Multi-platform
- Community-Driven

## Download and Installation

* [Flathub](https://flathub.org/apps/details/io.gitlab.librewolf-community)
* [AppImage](https://gitlab.com/librewolf-community/browser/appimage/-/releases)
* [Arch](https://gitlab.com/librewolf-community/browser/arch/-/releases) / [AUR](https://aur.archlinux.org/packages/?SeB=n&K=librewolf)
* [Gentoo](https://gitlab.com/librewolf-community/browser/gentoo)
* [Binary Tarball](https://gitlab.com/librewolf-community/browser/linux/-/releases)
* [macOS](https://gitlab.com/librewolf-community/browser/macos)
* [Windows](https://gitlab.com/librewolf-community/browser/windows)
* [Debian – OBS repo](https://software.opensuse.org//download.html?project=home%3Abgstack15%3Aaftermozilla&package=librewolf)

## Roadmap

Head over to [our GitLab repositories](https://gitlab.com/librewolf-community) or [our Gitter room](https://gitter.im/librewolf-community/librewolf)!

- https://gitlab.com/librewolf-community/settings/issues
- https://gitlab.com/librewolf-community/browser/linux/issues

## Contributions

LibreWolf is a largely community-driven project and we don't just say that. We encourage you to join us in [our Gitter room](https://gitter.im/librewolf-community/librewolf).

Merge requests, bug reports, and feature requests are welcome in all our repositories. We value discussion, debate, and feedback.

- [Settings](https://gitlab.com/librewolf-community/settings)
- [Browser](https://gitlab.com/librewolf-community/browser)
    - [Linux](https://gitlab.com/librewolf-community/browser/linux)
    - [Arch](https://gitlab.com/librewolf-community/browser/arch)
    - [Debian](https://gitlab.com/librewolf-community/browser/Debian)
    - [Flatpak](https://gitlab.com/librewolf-community/browser/flatpak)
    - [AppImage](https://gitlab.com/librewolf-community/browser/appimage)
    - [Gentoo](https://gitlab.com/librewolf-community/browser/gentoo)
    - [macOS](https://gitlab.com/librewolf-community/browser/macos)
    - [Windows](https://gitlab.com/librewolf-community/browser/windows)
- [Docs](https://gitlab.com/librewolf-community/docs)
- [Librewolf-community.gitlab.io](https://gitlab.com/librewolf-community/librewolf-community.gitlab.io)

If you wish to contribute a build for a currently unsupported operating system, we will do our best to support you.

## License

[Mozilla Public License 2.0](https://gitlab.com/librewolf-community/librewolf/blob/master/LICENSE)

## Acknowledgements

LibreWolf is based on [Librefox](https://github.com/intika/Librefox), whose development seems to have halted abruptly. We thank [@intika](https://github.com/intika) and Librefox contributors for their work.

### Arkenfox

- This is **not** Arkenfox
- Arkenfox is a _template_ user.js, fully documented, and the gold-standard on relevant Firefox preferences
- We rely heavily on Arkenfox's expertise, research, and knowledge: but we choose out own default preferences configuration
- We endeavor to keep up to date with Arkenfox

### Other acknowledgements

- The Mozilla team for their work on Firefox
- [@pyllyukko](https://github.com/pyllyukko) for compiling their user.js file

<small>Icons were created with images by [OpenClipart-Vectors](https://pixabay.com/users/OpenClipart-Vectors-30363) from [Pixabay](https://pixabay.com) and from <a href="https://www.flaticon.com/authors/vaadin" title="Vaadin">Vaadin</a> on <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></small>
